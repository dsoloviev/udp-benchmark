package com.opsec;

import java.io.IOException;
import java.net.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.DatagramChannel;
import java.time.Duration;
import java.time.Instant;

public class NIOReceiver {

    private final DatagramChannel channel;
    private final static ByteBuffer byteBuffer = ByteBuffer.allocate(256).order(ByteOrder.LITTLE_ENDIAN);

    public NIOReceiver() throws IOException {

        InetSocketAddress address = new InetSocketAddress("127.0.0.1", 6882);
        channel = DatagramChannel.open();
        channel.socket().bind(address);

    }

    public void Start() throws Exception {
        System.out.print("Java udp receive benchmark\n");
        System.out.print("Waiting for configuration packet from sender\n");

        channel.receive(byteBuffer);

        if (byteBuffer.position() != 20) {
            throw new Exception("Invalid Header");
        }

        var magic = byteBuffer.getLong(0);
        if (magic != 0x0123456789ABCDEFL) {
            throw new Exception("Invalid configuration package magic");
        }
        var size = byteBuffer.getLong(8);
        var ackInterval = byteBuffer.getInt(16);

        System.out.printf("Receiving %d packets with ACK interval %d...\n", size, ackInterval);

        var start = Instant.now();
        long checksum = BenchmarkLoop(size, ackInterval);
        var end = Instant.now();
        var nano_elapsed = Duration.between(start, end).getSeconds() * 1000000000L + Duration.between(start, end).getNano();


        System.out.printf("Checksum %s = %d\n", (checksum == 59882814 ? "passed" : "failed"), checksum);
        System.out.printf("Elapsed = %d\n", nano_elapsed / 1000000);
        System.out.printf("PPS = %d\n", size * 1000000000L / nano_elapsed);
    }

    private long BenchmarkLoop(long count, int ackInterval) throws IOException {

        long checksum = 0;
        SocketAddress addr;
        for (long i = 0; i < count; ++i) {

            long pktChecksum = 0;
            byteBuffer.clear();
            addr = channel.receive(byteBuffer);

            for (int j = 0; j < byteBuffer.position(); ++j) {
                pktChecksum += Byte.toUnsignedInt(byteBuffer.get(j));
            }
            checksum += pktChecksum % 13;
            if (i != 0 && (i + 1) % ackInterval == 0) {
                channel.send(byteBuffer, addr);
            }
        }
        return checksum;
    }


    public static void main(String[] args) throws Exception {
        var n = new NIOReceiver();
        n.Start();
    }
}

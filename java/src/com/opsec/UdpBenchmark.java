package com.opsec;

import java.io.*;
import java.net.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.time.Duration;
import java.time.Instant;

public class UdpBenchmark {
    private final DatagramSocket socket;
    private final byte[] buffer = new byte[256];

    public UdpBenchmark() throws SocketException {
        socket = new DatagramSocket(null);
        InetSocketAddress address = new InetSocketAddress("127.0.0.1", 6882);
        socket.bind(address);
    }

    public void Start() throws Exception {
        System.out.print("Java udp receive benchmark\n");
        System.out.print("Waiting for configuration packet from sender\n");

        DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
        socket.receive(packet);
        if (packet.getLength() != 20) {
            throw new Exception("Invalid configuration package size");
        }
        var data = packet.getData();
        var bb = ByteBuffer.wrap(data, 0, 20);
        bb.order(ByteOrder.LITTLE_ENDIAN);
        var magic = bb.getLong(0);
        if (magic != 0x0123456789ABCDEFL) {
            throw new Exception("Invalid configuration package magic");
        }
        var size = bb.getLong(8);
        var ack_interval = bb.getInt(16);

        System.out.printf("Receiving %d packets with ACK interval %d...\n", size, ack_interval);
        var start = Instant.now();
        long checksum = BenchmarkLoop(size, ack_interval);
        var end = Instant.now();
        var nano_elapsed = Duration.between(start, end).getSeconds() * 1000000000L + Duration.between(start, end).getNano();

        System.out.printf("Checksum %s = %d\n", (checksum == 59882814 ? "passed" : "failed"), checksum);
        System.out.printf("Elapsed = %d\n", nano_elapsed / 1000000);
        System.out.printf("PPS = %d\n", size * 1000000000L / nano_elapsed);
    }

    private long BenchmarkLoop(long count, int ack_interval) throws IOException {
        long checksum = 0;
        DatagramPacket request = new DatagramPacket(buffer, buffer.length);
        for (long i = 0; i < count; ++i) {
            long pktChecksum = 0;
            socket.receive(request);
            for (int j = 0; j < request.getLength(); ++j) {

                pktChecksum += Byte.toUnsignedInt(buffer[j]);
            }
            checksum += pktChecksum % 13;
            if (i != 0 && (i + 1) % ack_interval == 0) {
                DatagramPacket response = new DatagramPacket(buffer, 1, request.getAddress(), request.getPort());
                socket.send(response);
            }
        }
        return checksum;
    }
}
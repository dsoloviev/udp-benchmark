﻿using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Reflection.PortableExecutable;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UdpReceiver;

var socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
socket.Bind(new IPEndPoint(IPAddress.Loopback, 6882));
var buffer = new byte[256];

// Receive packet with config
Console.WriteLine("Waiting for configuration packet from sender");
var size = await socket.ReceiveAsync(buffer, SocketFlags.None);

if (size != 20)
{
    Console.Error.Write("Invalid configuration packet size");
    return -1;
}

var config = MemoryMarshal.Cast<byte, ConfigurationPacket>(buffer)[0];

if (config.Magic != 0x0123456789ABCDEFL)
{
    Console.Error.Write("Invalid configuration packet magic");
    return -1;
}

Console.WriteLine("Receiving {0} packets with ACK packet every {1} packets", config.PacketCount,config.AckInterval);

await Bench.RunBenchmark(config.PacketCount, config.AckInterval, socket);


return 0;


internal static class Bench
{
    [AsyncMethodBuilder(typeof(PoolingAsyncValueTaskMethodBuilder))]
    [MethodImpl(MethodImplOptions.AggressiveOptimization)]
    internal static async ValueTask RunBenchmark(long packetCount, int ackInterval, Socket socket)
    {
        // socket.Blocking = false;
        var buffer = new byte[256];
        EndPoint endPoint = new IPEndPoint(0, 0);
        var checksum = 0L;
        var sw = new Stopwatch();
        sw.Start();
        var sbuffer = new byte[1];
        var mem = new Memory<byte>(buffer);
        for (long i = 0; i < packetCount; ++i)
        {
            //var info = await socket.ReceiveFromAsync(mem); // buffer;
            var info = await socket.ReceiveFromAsync(buffer, SocketFlags.None, endPoint, default);
            var receivedBytes = info.ReceivedBytes;
            // endPoint = info.RemoteEndPoint;
            // var receivedBytes = socket.ReceiveFrom(buffer, SocketFlags.None, ref endPoint);
    
            var pktChecksum = 0L;
            for (var j = 0; j < receivedBytes; ++j)
            {
                pktChecksum += buffer[j];
            }

            checksum += pktChecksum % 13;
            
            if (i != 0 && (i + 1) % ackInterval == 0)
            {
                await socket.SendToAsync(sbuffer, SocketFlags.None, info.RemoteEndPoint);
            }
        }
        sw.Stop();

        Console.WriteLine("Elapsed = {0}", sw.ElapsedMilliseconds);
        Console.WriteLine("PPS = {0}", (Stopwatch.Frequency * packetCount) / sw.ElapsedTicks);
    }
}

[StructLayout(LayoutKind.Sequential, Pack = 1)]
internal struct ConfigurationPacket
{
    public long Magic;
    public long PacketCount;
    public int AckInterval;
}
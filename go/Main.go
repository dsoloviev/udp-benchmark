package main

import (
	"encoding/binary"
	"errors"
	"fmt"
	"net"
	"time"
)

func main() {

	fmt.Println("Udp Receiver (golang) Started")
	fmt.Println("Waiting for configuration packet from sender")

	conn, _ := net.ListenPacket("udp", ":6882")
	buf := make([]byte, 256)
	n, _, _ := conn.ReadFrom(buf)

	if n != 20 {
		errors.New("Wrong packet length")
		return
	}

	magic := binary.LittleEndian.Uint64(buf[0:8])

	if magic != 0x0123456789ABCDEF {
		errors.New("Wrong packet magic")
		return
	}

	packets := binary.LittleEndian.Uint64(buf[8:16])
	ackInterval := binary.LittleEndian.Uint32(buf[16:20])
	fmt.Println(fmt.Sprintf("Receiving  %d  packets with ACK every %d packets", packets, ackInterval))

	start := time.Now()
	checksum := 0
	for i := uint64(0); i < packets; i++ {

		pktChecksum := 0
		n, dst, _ := conn.ReadFrom(buf)
		for j := 0; j < n; j++ {
			pktChecksum += int(buf[j])
		}
		checksum += pktChecksum % 13
		if i != 0 && ((i+1)%uint64(ackInterval)) == 0 {
			conn.WriteTo(buf, dst)
		}
	}

	if checksum == 59882814 {
		fmt.Println("Checksum passed ")
	} else {
		fmt.Println("Checksum failed ")
	}

	fmt.Println("Elapsed = ", time.Now().Sub(start).Seconds())
	fmt.Println("PPS = ", packets/uint64(time.Now().Sub(start).Seconds()))

}

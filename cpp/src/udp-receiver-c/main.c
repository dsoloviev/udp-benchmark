#define _GNU_SOURCE

#include "../udp-sender/ConfigurationPacket.hpp"

#include <stdio.h> //printf
#include <string.h> //memset
#include <stdlib.h> //exit(0);
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>
#include <errno.h>

#define SERVER "127.0.0.1"
#define BATCH_SIZE 32
#define MESSAGE_SIZE 256
#define PORT 6882   //The port on which to send data

void die(char *s) {
    perror(s);
    exit(1);
}

int guard(ssize_t n, char *err) {
    if (n == -1) {
        perror(err);
        exit(1);
    }
    return n;
}

int64_t diff_usec(struct timeval start, struct timeval end) {
    int64_t start_usec = start.tv_sec * 1000000ll + start.tv_usec;
    int64_t end_usec = end.tv_sec * 1000000ll + end.tv_usec;
    return end_usec - start_usec;
}

int main(int argc, const char **argv) {
    printf("Udp Receiver (C with recvmmsg) Started\n");
    int s = guard(socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP), "Couldn't create socket");

    struct sockaddr_in addr_listen;

    memset((char *) &addr_listen, 0, sizeof(addr_listen));
    addr_listen.sin_family = AF_INET;
    addr_listen.sin_port = htons(PORT);

    if (inet_aton(SERVER, &addr_listen.sin_addr) == 0) {
        fprintf(stderr, "inet_aton() failed\n");
        exit(1);
    }

    guard(bind(s, &addr_listen, sizeof(addr_listen)), "Couldn't bind socket");

    struct ConfigurationPacket config = {};

    printf("Waiting for configuration packet from sender\n");
    memset((char *) &addr_listen, 0, sizeof(addr_listen));
    socklen_t addr_listen_size = 0;


    ssize_t config_size = guard(
            recvfrom(s, (void *) &config, sizeof(struct ConfigurationPacket), 0, (struct sockaddr *) &addr_listen,
                     &addr_listen_size), "Couldn't receive config packet");

    if (config_size != sizeof(struct ConfigurationPacket)) {
        die("Invalid configuration packet size");
    }

    if (config.Magic != ConfigurationPacketMagic) {
        die("Invalid configuration packet magic");
    }

    printf("Receiving %ld packets with ACK every %d packets\n", config.PacketCount, config.AckInterval);

    int flags = guard(fcntl(s, F_GETFL), "could not get flags on UDP listening socket");
    guard(fcntl(s, F_SETFL, flags | O_NONBLOCK), "could not set UDP listening socket to be non-blocking");

    struct sockaddr_in from;
    memset((char *) &from, 0, sizeof(from));
    unsigned int from_len = 0;

    struct mmsghdr headers[BATCH_SIZE] __attribute__ ((aligned (16)));
    struct sockaddr_in froms[BATCH_SIZE] __attribute__ ((aligned (16)));
    struct iovec payloads[BATCH_SIZE] __attribute__ ((aligned (16)));
    unsigned char *buffers = aligned_alloc(16, MESSAGE_SIZE * BATCH_SIZE);
    memset(headers, 0, sizeof(headers));
    memset(headers, 0, sizeof(payloads));
    for (int i = 0; i < BATCH_SIZE; ++i) {
        payloads[i].iov_base = buffers + i * MESSAGE_SIZE;
        payloads[i].iov_len = 256;
        headers[i].msg_hdr.msg_iov = payloads + i;
        headers[i].msg_hdr.msg_iovlen = 1;
        headers[i].msg_hdr.msg_name = froms + i;
        headers[i].msg_hdr.msg_namelen = sizeof(struct sockaddr_in);
    }

    int i = 0;

    struct timespec timeout;
    timeout.tv_sec = 5;
    timeout.tv_nsec = 0;

    int64_t checksum = 0;
    struct timeval start;
    gettimeofday(&start, NULL);
    int msg_count = 0;
    struct timespec sleep_interval = {.tv_sec = 0, .tv_nsec = 1000};

    while (i < config.PacketCount) {
        while(1) {
            msg_count = recvmmsg(s, headers, BATCH_SIZE, 0, &timeout);
            if(msg_count >= 0) break;
            if(errno == EWOULDBLOCK) {
                nanosleep(&sleep_interval, NULL);
                continue;
            }
            die("Coudln't read with recvmmsg");
        }

        for (int j = 0; j < msg_count; ++j) {

            int64_t pkt_checksum = 0;

            for (int k = 0; k < headers[j].msg_len; ++k) {
                pkt_checksum += (int64_t) buffers[j * MESSAGE_SIZE + k];
            }

            headers[j].msg_len = 0;
            checksum += pkt_checksum % 13;
        }

        i += msg_count;

        if (i != 0 && i % config.AckInterval == 0) {
            int dummy = 0;
            sendto(s, (char *) &dummy, 1, 0, froms, sizeof(struct sockaddr_in));
        }
    }
    struct timeval end;
    gettimeofday(&end, NULL);
    int64_t elapsed = diff_usec(start, end);
    printf("Checksum %s = %ld\n", checksum == 59882814l ? "passed" : "failed", checksum);
    printf("Execution time = %d ms; PPS = %d\n", (int) (elapsed / 1000), (int) (1000000ll * i / elapsed));
    printf("Done\n");
    close(s);

    free(buffers);

    return 0;
}
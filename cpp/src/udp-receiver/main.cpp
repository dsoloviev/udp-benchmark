#include "UdpSynchronousReceiveBenchmark.hpp"
#include "UdpAsyncReceiveBenchmark.hpp"

#include <iostream>
#include <boost/lexical_cast.hpp>

int main(int argc, char **argv) {
    std::cout << "Udp Receiver (C++/boost::asio) started" << std::endl;

    bool async = true;

    if (argc > 1) {
        std::string_view type(argv[1]);
        if (type == "sync") {
            async = false;
        } else if (type == "async") {
            async = true;
        } else {
            std::cerr << "Invalid type, must be async or sync" << std::endl;
            return 1;
        }

    }

    if (async) {
        int thread_count = 0;
        if (argc == 3) {
            thread_count = boost::lexical_cast<int>(argv[2]);
        }
        UdpAsyncReceiveBenchmark bench(thread_count);
        bench.Start();
    } else {
        UdpSynchronousReceiveBenchmark bench;
        bench.Start();
    }

    return 0;
}

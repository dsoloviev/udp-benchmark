//
// Created by dmitry on 11/21/21.
//
#include "../udp-sender/ConfigurationPacket.hpp"
#include "UdpAsyncReceiveBenchmark.hpp"
#include <iostream>
#include <cstdint>

namespace asio = boost::asio;
using asio::ip::udp;

UdpAsyncReceiveBenchmark::UdpAsyncReceiveBenchmark(int thread_count) :
        m_thread_count(thread_count),
        m_ios(thread_count == 0 ? BOOST_ASIO_CONCURRENCY_HINT_UNSAFE
                                : BOOST_ASIO_CONCURRENCY_HINT_DEFAULT), // BOOST_ASIO_CONCURRENCY_HINT_UNSAFE_IO
        m_dummy_work(m_ios),
        m_socket(m_ios),
        m_from(),
        m_buffer(65536, 0),
        m_expected_packets(0),
        m_received_packets(0),
        m_checksum(0) {
    m_socket.open(boost::asio::ip::udp::v4());
    m_socket.bind(udp::endpoint(asio::ip::address_v4::any(), 6882u));

    for (int i = 0; i < thread_count - 1; ++i) {
        m_worker_threads.emplace_back(std::thread([this] { m_ios.run(); }));
    }
}

UdpAsyncReceiveBenchmark::~UdpAsyncReceiveBenchmark() {
    m_ios.stop();
    for (auto &t: m_worker_threads) {
        t.join();
    }
}

void UdpAsyncReceiveBenchmark::Start() {
    std::chrono::steady_clock::time_point start = {};
    std::cout << "async_receive_from benchmark (asio thread pool size = " << m_thread_count << ")" << std::endl;
    std::cout << "Waiting for configuration packet from sender" << std::endl;
    m_socket.async_receive(
            boost::asio::buffer(m_buffer),
            [this, &start](const boost::system::error_code &ec, size_t received_bytes) {
                if (ec) {
                    std::cerr << "Receive failed" << ec.message() << std::endl;
                    std::terminate();
                }

                if (received_bytes != sizeof(ConfigurationPacket)) {
                    std::cerr << "Invalid configuration packet size" << std::endl;
                    std::terminate();
                }

                const auto pPacket = reinterpret_cast<ConfigurationPacket *>(m_buffer.data());

                if (pPacket->Magic != ConfigurationPacketMagic) {
                    std::cerr << "Invalid configuration packet magic" << std::endl;
                    std::terminate();
                }

                m_expected_packets = pPacket->PacketCount;
                m_ack_interval = pPacket->AckInterval;
                std::cout << "Receiving " << m_expected_packets << " packets with ACK packet every " << m_ack_interval
                          << " packets" << std::endl;
                start = std::chrono::steady_clock::now();
                ListenAsync();
            }
    );

    m_ios.run();

    const auto end = std::chrono::steady_clock::now();
    std::cout << "Received " << m_received_packets << " packets"
              << "; checksum " << (m_checksum == 59882814 ? "passed" : "failed") << " = " << m_checksum
              << std::endl;


    const auto execution_ns = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();
    std::cout << "Execution time = " << execution_ns / 1000000 << "ms"
              << "; PPS = " << (1e9 * m_received_packets) / execution_ns
              << std::endl;
}

void UdpAsyncReceiveBenchmark::OnData(const boost::system::error_code &ec, size_t received_bytes) {
    if (ec) {
        std::cerr << "Receive failed" << ec.message() << std::endl;
        std::terminate();
    }

    ++m_received_packets;
    size_t pkt_checksum = 0;
    for (auto j = 0; j < received_bytes; ++j) {
        pkt_checksum += m_buffer.at(j);
    }
    m_checksum += pkt_checksum % 13;

    if ((m_received_packets) % m_ack_interval == 0) {
        m_socket.async_send_to(
                boost::asio::buffer(m_buffer.data(), 1),
                m_from,
                [this](const auto &ec, const auto sent) {
                    ListenAsync();
                }
        );
    } else {
        ListenAsync();
    }
}

void UdpAsyncReceiveBenchmark::ListenAsync() {
    if (m_received_packets >= m_expected_packets) {
        m_ios.stop();
        return;
    }

    m_socket.async_receive_from(
            boost::asio::buffer(m_buffer),
            m_from,
            [this](auto &&PH1, auto &&PH2) {
                OnData(std::forward<decltype(PH1)>(PH1), std::forward<decltype(PH2)>(PH2));
            }
    );
}

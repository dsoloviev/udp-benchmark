//
// Created by dmitry on 11/21/21.
//

#include "../udp-sender/ConfigurationPacket.hpp"
#include "UdpSynchronousReceiveBenchmark.hpp"
#include "aligned_vector.hpp"
#include <iostream>


namespace asio = boost::asio;
using asio::ip::udp;


UdpSynchronousReceiveBenchmark::UdpSynchronousReceiveBenchmark():
    m_ios(BOOST_ASIO_CONCURRENCY_HINT_UNSAFE_IO),
    m_socket(m_ios)
{
    m_socket.open(boost::asio::ip::udp::v4());
    m_socket.bind(udp::endpoint(asio::ip::address_v4::any(), 6882u));
}

void UdpSynchronousReceiveBenchmark::Start() {
    std::cout << "receive_from (sync) benchmark" << std::endl;
    std::cout << "Waiting for configuration packet from sender" << std::endl;
    ConfigurationPacket config = {};
    auto size = m_socket.receive_from(asio::mutable_buffer(&config, sizeof(ConfigurationPacket)), m_from);
    if(size != sizeof(ConfigurationPacket)) {
        throw std::runtime_error("Invalid configuration packet size");
    }

    if(config.Magic != ConfigurationPacketMagic) {
        std::cerr << "Invalid configuration packet magic" << std::endl;
        std::terminate();
    }

    std::cout << "Receiving " << config.PacketCount << " packets with ACK packet every "
        << config.AckInterval << " packets" << std::endl;

    BenchmarkLoop(config.PacketCount, config.AckInterval);
}

void UdpSynchronousReceiveBenchmark::BenchmarkLoop(size_t expected_packets, int ack_interval) {
    auto buffer = aligned_vector<uint8_t>(65536, 0);
    auto bufferData = buffer.data();
    udp::endpoint from = {};
    const auto start = std::chrono::steady_clock::now();
    size_t total_size = 0;
    size_t checksum = 0;
    long i = 0;

    for (i = 0; i < expected_packets; ++i) {
        const auto size = m_socket.receive_from(asio::mutable_buffer(bufferData, 65535), from);
        size_t pkt_checksum = 0;
        for (auto j = 0; j < size; ++j) {
            pkt_checksum += buffer.at(j);
        }
        checksum += pkt_checksum % 13;
        total_size += size;

        if(i != 0 && (i + 1) % ack_interval == 0) {
            m_socket.send_to(asio::buffer(bufferData, 1), from); // ACK every 4K packets
        }
    }

    const auto end = std::chrono::steady_clock::now();
    std::cout << "Received " << i << " packets, total size = " << total_size
              << "; checksum " << (checksum == 59882814 ? "passed" : "failed") << " = " << checksum
              << std::endl;


    const auto execution_ns = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();
    std::cout << "Execution time = " << execution_ns / i << "ms"
              << "; PPS = " << (1e9 * i) / execution_ns
              << std::endl;
}

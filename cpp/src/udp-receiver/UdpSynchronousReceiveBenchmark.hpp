#pragma once

#include <boost/asio.hpp>

class UdpSynchronousReceiveBenchmark final {
public:
    UdpSynchronousReceiveBenchmark();

    void Start();
private:

    void BenchmarkLoop(size_t expected_packets, int ack_interval);

    boost::asio::io_context m_ios;
    boost::asio::ip::udp::socket m_socket;
    boost::asio::ip::udp::endpoint m_from;
};

#pragma once

#include "aligned_vector.hpp"

#include <boost/asio.hpp>
#include <atomic>

class UdpAsyncReceiveBenchmark final {
public:
    explicit UdpAsyncReceiveBenchmark(int thread_count = 0);
    ~UdpAsyncReceiveBenchmark();
    void Start();
private:
    void OnData(const boost::system::error_code &ec, size_t received_bytes);

    void ListenAsync();

    int m_thread_count;
    boost::asio::io_context m_ios;
    std::vector<std::thread>    m_worker_threads;
    boost::asio::io_context::work m_dummy_work;
    boost::asio::ip::udp::socket m_socket;
    boost::asio::ip::udp::endpoint m_from;
    aligned_vector<uint8_t> m_buffer;
    size_t m_expected_packets;
    size_t m_ack_interval;
    size_t m_received_packets;
    size_t m_checksum;
};

#pragma once

#include <boost/align/aligned_allocator.hpp>
#include <vector>


template<typename element_t>
using aligned_vector = std::vector<element_t, boost::alignment::aligned_allocator<element_t, 4>>;
#pragma once

#define ConfigurationPacketMagic 0x0123456789ABCDEFll

#pragma pack(push, 1)
struct ConfigurationPacket {
    long Magic;
    long PacketCount;
    int AckInterval;
};
#pragma pack(pop)
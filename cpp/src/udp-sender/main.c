#define _GNU_SOURCE

#include "ConfigurationPacket.hpp"

#include <stdio.h> //printf
#include <string.h> //memset
#include <stdlib.h> //exit(0);
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <unistd.h>

#define SERVER "127.0.0.1"
#define BATCH_SIZE 128
#define MESSAGE_SIZE 256
#define PORT 6882   //The port on which to send data

void die(char *s) {
    perror(s);
    exit(1);
}

int guard(ssize_t n, char *err) {
    if (n == -1) {
        perror(err);
        exit(1);
    }
    return n;
}

int main(int argc, const char **argv) {
    printf("Udp Sender (C with sendmmsg)\n");
    int s = guard(socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP), "Couldn't create socket");

    struct sockaddr_in si_other;
    int slen = sizeof(si_other);

    memset((char *) &si_other, 0, sizeof(si_other));
    si_other.sin_family = AF_INET;
    si_other.sin_port = htons(PORT);

    if (inet_aton(SERVER, &si_other.sin_addr) == 0) {
        fprintf(stderr, "inet_aton() failed\n");
        exit(1);
    }

    struct ConfigurationPacket config = {
            ConfigurationPacketMagic,
            10000000l,
            BATCH_SIZE
    };
    printf("Sending config packet\n");
    if (sendto(s, (void *) &config, sizeof(struct ConfigurationPacket), 0, (struct sockaddr *) &si_other, slen) == -1) {
        die("Couldn't send config packet");
    }

    printf("Sending %ld packets...\n", config.PacketCount);
    struct sockaddr_in from;
    memset((char *) &from, 0, sizeof(from));
    unsigned int from_len = 0;

    struct mmsghdr headers[BATCH_SIZE];
    struct iovec payloads[BATCH_SIZE];
    char *buffers = aligned_alloc(16, MESSAGE_SIZE * BATCH_SIZE);
    memset(headers, 0, sizeof(headers));
    memset(headers, 0, sizeof(payloads));
    for (int i = 0; i < BATCH_SIZE; ++i) {

        payloads[i].iov_base = buffers + i * MESSAGE_SIZE;
        payloads[i].iov_len = 256;

        headers[i].msg_hdr.msg_iov = payloads + i;
        headers[i].msg_hdr.msg_iovlen = 1;
        headers[i].msg_hdr.msg_name = &si_other;
        headers[i].msg_hdr.msg_namelen = sizeof(si_other);
    }

    for (int i = 0; i < config.PacketCount; i += BATCH_SIZE) {
        for (int j = 0; j < BATCH_SIZE; ++j) {
            memset(buffers + j * MESSAGE_SIZE, i + j, MESSAGE_SIZE);
        }

        // Send all messages in the batch at once
        guard(sendmmsg(s, headers, BATCH_SIZE, 0), "Couldn't send");

        // Wait for ACK for the batch
        if (i % config.AckInterval == 0) {
            int dummy;
            guard(recvfrom(s, &dummy, sizeof(dummy), 0, (struct sockaddr *) &from, &from_len), "Coudln't receive ack");
        }
    }

    printf("Done\n");
    close(s);

    free(buffers);

    return 0;
}
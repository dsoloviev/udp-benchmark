Setting socket buffers to 10Mb
net.core.rmem_max = 10485760
net.core.rmem_default = 10485760
net.core.wmem_max = 10485760
net.core.wmem_default = 10485760
===============================================
Java udp receive benchmark
Waiting for configuration packet from sender
Udp Sender (c++)
Sending config packet
Sending 10000000 packets...
Receiving 10000000 packets...
Done
Checksum passed = 59882814
Elapsed = 51831
PPS = 192931
===============================================
Waiting for configuration packet from sender
Udp Sender (c++)
Sending config packet
Sending 10000000 packets...
Receiving 10000000 packets
Done
Elapsed = 35828
PPS = 279105
===============================================
Udp Receiver started
async_receive_from benchmark (asio thread pool size = 2)
Waiting for configuration packet from sender
Udp Sender (c++)
Sending config packet
Sending 10000000 packets...
Receiving 10000000 packets...
Done
Received 10000000 packets; checksum passed = 59882814
Execution time = 2651ms; PPS = 377121
===============================================
Udp Receiver started
receive_from (sync) benchmark
Waiting for configuration packet from sender
Udp Sender (c++)
Sending config packet
Sending 10000000 packets...
Receiving 10000000 packets...
Done
Received 10000000 packets, total size = 2560000000; checksum passed = 59882814
Execution time = 2686ms; PPS = 372275

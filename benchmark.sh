#!/bin/sh
PERF="perf stat -B -e cache-references,cache-misses,cycles,instructions,branches,faults,migrations "

if [ $(id -u) -ne 0 ]; then
    echo 'Run this script with sudo'
    exit -1
fi

echo Ensure buffers are big enough

[ $(sysctl -n net.core.rmem_max) -lt 65536 ] && sysctl -w net.core.rmem_max=65536
[ $(sysctl -n net.core.rmem_default) -lt 65536 ] && sysctl -w net.core.rmem_default=65536
[ $(sysctl -n net.core.wmem_max) -lt 65536 ] && sysctl -w net.core.wmem_max=65536
[ $(sysctl -n net.core.wmem_default) -lt 65536 ] && sysctl -w net.core.wmem_default=65536

echo ===============================================

sudo $PERF taskset --cpu-list 0,6 /home/dmitry/.jdks/openjdk-16.0.1/bin/java -da -dsa -classpath java/out/production/java com.opsec.Main &
PID=$!
sleep 1
taskset --cpu-list 1,7 cpp/cmake-build-release/udp-sender
wait 
sleep 2

echo ===============================================

sudo $PERF taskset --cpu-list 0,6 dotnet/UdpReceiver/bin/Release/net6.0/UdpReceiver &
PID=$!
sleep 1
taskset --cpu-list 1,7 cpp/cmake-build-release/udp-sender
wait
sleep 2

echo ===============================================

sudo $PERF taskset --cpu-list 0,6 cpp/cmake-build-release/udp-receiver &
PID=$!
sleep 1
taskset --cpu-list 1,7 cpp/cmake-build-release/udp-sender
wait
